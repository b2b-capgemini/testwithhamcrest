package com.capgemini.TestWithHamcrest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import static org.hamcrest.Matchers.*;
import org.junit.Before;
import org.junit.Test;

public class TestHamcrestOnSimpleObject {

	private Krzeslo krzeslo;
	@Before
	public void init() {
		krzeslo = new Krzeslo(4,"Dąb", "brązowy");
		//uzupełniać właściwości krzesła można na dwa sposoby 
		//1. od razu stworzyć odpowiedni konstruktor ze wszystkimi własciowściami
		//2. za pomocą getterow i setterow
		//wybieram opcje 1;
	}
	
	@Test
	public void sprawdz_czy_podane_krzeslo_ma_kolor_brazowy() {
		
		assertThat(krzeslo, hasProperty("kolor", equalTo("brązowy")));
	}
	
	@Test
	public void sprawdz_czy_liczba_nog_w_krzesle_jest_rowna_4() {
		assertThat(krzeslo, hasProperty("iloscNog",equalTo(4)));
	}
	
	//dla potrzeb testera automatycznego tylko takie porównania są najbardziej potrzebne na początek
	//wrócmy jeszcze na moment do list
}
