package com.capgemini.TestWithHamcrest;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class TestListWithHamcrest {

	@Test
	public void sprawdz_czy_w_liscie_wystepuje_liczba_2() {
		List<Integer> liczby =  new ArrayList<Integer>();
		liczby.add(2);
		liczby.add(4);
		liczby.add(5);
		
		assertThat(liczby, hasItem(2));
	}
	
	@Test
	public void sprawdz_czy_w_liscie_z_napisami_jest_wyraz_zielony() {
		List<String> kolory = Arrays.asList("zielony", "pomarańczowy", "żółty","czerwony");
		
		//metoda contains pochodzi z hamcrest jak jest pokazane dokładnie jest z paczki Matchers
		//pierwsze rozwiązanie dopisać instancje klasy Matchers 
		//lub drugie dodać na stałe statyczny import Matchers.*

		assertThat(kolory, hasItems("zielony"));
	}
	
	@Test
	public void sprawdz_czy_kolekcja_zawiera_dokladnie_wskazane_elementy() {
		List<String> kolory = Arrays.asList("zielony", "pomarańczowy", "żółty","czerwony");
		
//		assertThat(kolory, hasItems("zielony", "pomarańczowy", "żółty", "czerwony"));
		
		//jesli znamy dokładnie elementy ale nie znamy ich kolejności
		assertThat(kolory, containsInAnyOrder("pomarańczowy","zielony",  "żółty", "czerwony"));
	}
	@Test
	public void sprawdzmy_czy_elementy_kolekcji_z_liczbami_sa_wieksze_od_10() {
		List<Integer> liczby = new ArrayList<Integer>();
		liczby.add(20);
		liczby.add(40);
		
		assertThat(liczby, everyItem(greaterThan(10)));
	}
	@Test
	public void spradzmy_czy_podana_kolekcja_nie_jest_pusta() {
		List<Integer> numery = Arrays.asList(1,2,3);
		
		assertThat(numery, not(empty()));
	}
	
	@Test
	public void sprawdzmy_czy_podana_lista_jest_pusta() {
		List<Integer> pustaLista = new ArrayList<Integer>();
		
		assertThat(pustaLista, is(empty()));
	}
	
	@Test
	public void sprawdzmy_czy_podana_lista_numerow_ma_wielkosc_3_i_jednoczesnie_zawiera_liczbe4() {
		List<Integer> liczby = new ArrayList<Integer>();
		liczby.add(2);
		liczby.add(3);
		liczby.add(4);
//		liczby.add(null);
		
		assertThat(liczby, both(hasSize(3)).and(notNullValue()));
		
	}
	
	

}
