package com.capgemini.TestWithHamcrest;

public class Krzeslo {

	private int iloscNog;
	private String rodzajDrewna;
	private String kolor;
	public int getIloscNog() {
		return iloscNog;
	}
	public Krzeslo(int iloscNog, String rodzajDrewna, String kolor) {
		super();
		this.iloscNog = iloscNog;
		this.rodzajDrewna = rodzajDrewna;
		this.kolor = kolor;
	}
	public void setIloscNog(int iloscNog) {
		this.iloscNog = iloscNog;
	}
	public String getRodzajDrewna() {
		return rodzajDrewna;
	}
	public void setRodzajDrewna(String rodzajDrewna) {
		this.rodzajDrewna = rodzajDrewna;
	}
	public String getKolor() {
		return kolor;
	}
	public void setKolor(String kolor) {
		this.kolor = kolor;
	}
}
